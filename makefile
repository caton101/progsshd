# special thanks to the following sources for fixing compiler issues:
# https://stackoverflow.com/questions/13707643/undefined-reference-to-ssh-new?noredirect=1
# https://stackoverflow.com/questions/4491350/undefined-reference-to-forkptyall:

compile:
	gcc main.c -Wall -lssh -lutil -o progsshd

all:
	make clean
	make keys
	make compile

keys:
	mkdir keys
	ssh-keygen -N "" -t dsa -f ./keys/ssh_host_dsa_key
	ssh-keygen -N "" -t rsa -f ./keys/ssh_host_rsa_key

clean:
	rm -rf keys progsshd

