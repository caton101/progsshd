/*
    ProgSSHD
    Copyright (C) 2021 Cameron Himes

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
    NOTE:
    This file was heavily modified by Cameron Himes. Here is a list of major changes:
    - removed all forms of authentication
    - allow user to set a port using flags
    - allow user to set address binding using flags
    - make address binding optional
    - make output more descriptive
    - allow the use of other programs besides BASH
    - allow multiple client connections

    original code: https://github.com/substack/libssh/blob/master/examples/samplesshd-tty.c
*/

/*
    Copyright 2003-2011 Aris Adamantiadis
    This file is part of the SSH Library
    You are free to copy this file, modify it in any way, consider it being public
    domain. This does not apply to the rest of the library though, but it is
    allowed to cut-and-paste working code from this file to any license of
    program.
    The goal is to show the API in action. It's not a reference on how terminal
    clients must be made or how a client should react.
*/

#include <libssh/libssh.h>
#include <libssh/server.h>
#include <libssh/callbacks.h>

#include <argp.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <poll.h>
#include <pty.h>
#include <sys/wait.h>
#include <signal.h>

#define KEYS_FOLDER "./keys/"

static int buffer_size = 2048;
static int port = 22;
static char *custom_executable = NULL;
static int is_executable_set = 0;
static char *default_executable = "./testsh";

static int authenticate(ssh_session session)
{
    ssh_message message;

    do
    {
        message = ssh_message_get(session);
        if (!message)
            break;
        switch (ssh_message_type(message))
        {
        case SSH_REQUEST_AUTH:
            switch (ssh_message_subtype(message))
            {
            case SSH_AUTH_METHOD_PASSWORD:
                printf("[INFO] User %s wants to auth with pass %s\n",
                       ssh_message_auth_user(message),
                       ssh_message_auth_password(message));
                ssh_message_auth_reply_success(message, 0);
                ssh_message_free(message);
                return 1;

            case SSH_AUTH_METHOD_NONE:
            default:
                printf("[INFO] User %s wants to auth with unknown auth %d\n",
                       ssh_message_auth_user(message),
                       ssh_message_subtype(message));
                ssh_message_auth_reply_success(message, 0);
                ssh_message_free(message);
                return 1;
            }
            break;
        default:
            ssh_message_auth_set_methods(message,
                                         SSH_AUTH_METHOD_PASSWORD |
                                             SSH_AUTH_METHOD_INTERACTIVE);
            ssh_message_reply_default(message);
        }
        ssh_message_free(message);
    } while (1);
    return 0;
}

static int copy_fd_to_chan(socket_t fd, int revents, void *userdata)
{
    ssh_channel chan = (ssh_channel)userdata;
    char buf[buffer_size];
    int sz = 0;

    if (!chan)
    {
        close(fd);
        return -1;
    }
    if (revents & POLLIN)
    {
        sz = read(fd, buf, buffer_size);
        if (sz > 0)
        {
            ssh_channel_write(chan, buf, sz);
        }
    }
    if (revents & POLLHUP)
    {
        ssh_channel_close(chan);
        sz = -1;
    }
    return sz;
}

static int copy_chan_to_fd(ssh_session session,
                           ssh_channel channel,
                           void *data,
                           uint32_t len,
                           int is_stderr,
                           void *userdata)
{
    int fd = *(int *)userdata;
    int sz;
    (void)session;
    (void)channel;
    (void)is_stderr;

    sz = write(fd, data, len);
    return sz;
}

static void chan_close(ssh_session session, ssh_channel channel, void *userdata)
{
    int fd = *(int *)userdata;
    (void)session;
    (void)channel;

    close(fd);
}

struct ssh_channel_callbacks_struct cb = {
    .userdata = NULL,
    .channel_data_function = copy_chan_to_fd,
    .channel_eof_function = chan_close,
    .channel_close_function = chan_close};

static int main_loop(ssh_channel chan)
{
    ssh_session session = ssh_channel_get_session(chan);
    socket_t fd;
    struct termios *term = NULL;
    struct winsize *win = NULL;
    pid_t childpid;
    ssh_event event;
    short events;

    childpid = forkpty(&fd, NULL, term, win);
    if (childpid == 0)
    {
        if (is_executable_set)
        {
            execl(custom_executable, "", (char *)NULL);
        }
        else
        {
            execl(default_executable, "", (char *)NULL);
        }
        exit(1);
    }

    cb.userdata = &fd;
    ssh_callbacks_init(&cb);
    ssh_set_channel_callbacks(chan, &cb);

    events = POLLIN | POLLPRI | POLLERR | POLLHUP | POLLNVAL;

    event = ssh_event_new();
    if (event == NULL)
    {
        printf("[ERROR] Failed to get an event.\n");
        return -1;
    }
    if (ssh_event_add_fd(event, fd, events, copy_fd_to_chan, chan) != SSH_OK)
    {
        printf("[ERROR] Couldn't add a fd to the event.\n");
        return -1;
    }
    if (ssh_event_add_session(event, session) != SSH_OK)
    {
        printf("[ERROR] Couldn't add the session to the event.\n");
        return -1;
    }

    do
    {
        ssh_event_dopoll(event, 1000);
    } while (!ssh_channel_is_closed(chan));

    ssh_event_remove_fd(event, fd);

    ssh_event_remove_session(event, session);

    ssh_event_free(event);
    return 0;
}

/* Program documentation. */
static char doc[] = "ProgSSHD -- a Secure Shell server that runs a specified program";

/* A description of the arguments we accept. */
static char args_doc[] = "";

/* The options we understand. */
static struct argp_option options[] = {
    {.name = "binding",
     .key = 'b',
     .arg = "BINDING",
     .flags = 0,
     .doc = "Set the address to bind.",
     .group = 0},
    {.name = "port",
     .key = 'p',
     .arg = "PORT",
     .flags = 0,
     .doc = "Set the port to bind.",
     .group = 0},
    {.name = "hostkey",
     .key = 'k',
     .arg = "FILE",
     .flags = 0,
     .doc = "Set the host key.",
     .group = 0},
    {.name = "dsakey",
     .key = 'd',
     .arg = "FILE",
     .flags = 0,
     .doc = "Set the dsa key.",
     .group = 0},
    {.name = "rsakey",
     .key = 'r',
     .arg = "FILE",
     .flags = 0,
     .doc = "Set the rsa key.",
     .group = 0},
    {.name = "executable",
     .key = 'e',
     .arg = "FILE",
     .flags = 0,
     .doc = "Set the program to execute.",
     .group = 0},
    {.name = "verbose",
     .key = 'v',
     .arg = NULL,
     .flags = 0,
     .doc = "Get verbose output.",
     .group = 0},
    {NULL, 0, 0, 0, NULL, 0}};

/* Parse a single option. */
static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
    /* Get the input argument from argp_parse, which we
   * know is a pointer to our arguments structure.
   */
    ssh_bind sshbind = state->input;

    switch (key)
    {
    case 'b':
        ssh_bind_options_set(sshbind, SSH_BIND_OPTIONS_BINDADDR, arg);
        break;
    case 'p':
        ssh_bind_options_set(sshbind, SSH_BIND_OPTIONS_BINDPORT_STR, arg);
        port = atoi(arg);
        break;
    case 'd':
        ssh_bind_options_set(sshbind, SSH_BIND_OPTIONS_DSAKEY, arg);
        break;
    case 'k':
        ssh_bind_options_set(sshbind, SSH_BIND_OPTIONS_HOSTKEY, arg);
        break;
    case 'r':
        ssh_bind_options_set(sshbind, SSH_BIND_OPTIONS_RSAKEY, arg);
        break;
    case 'v':
        ssh_bind_options_set(sshbind, SSH_BIND_OPTIONS_LOG_VERBOSITY_STR, "3");
        break;
    case 'e':
        custom_executable = arg;
        is_executable_set = 1;
        break;
    default:
        return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

/* Our argp parser. */
static struct argp argp = {options, parse_opt, args_doc, doc, NULL, NULL, NULL};

int run_server(int argc, char **argv)
{
    ssh_session session;
    ssh_bind sshbind;
    ssh_message message;
    ssh_channel chan = 0;
    int auth = 0;
    int shell = 0;
    int r;

    sshbind = ssh_bind_new();
    session = ssh_new();

    ssh_bind_options_set(sshbind, SSH_BIND_OPTIONS_DSAKEY,
                         KEYS_FOLDER "ssh_host_dsa_key");
    ssh_bind_options_set(sshbind, SSH_BIND_OPTIONS_RSAKEY,
                         KEYS_FOLDER "ssh_host_rsa_key");

    /*
     * Parse our arguments; every option seen by parse_opt will
     * be reflected in arguments.
     */
    argp_parse(&argp, argc, argv, 0, 0, sshbind);

    (void)argc;
    (void)argv;

    if (ssh_bind_listen(sshbind) < 0)
    {
        printf("[ERROR] Failed to bind to socket %s.\n", ssh_get_error(sshbind));
        return 1;
    }
    printf("[INFO] Started ProgSSHD on port %d.\n", port);
    while (1)
    {
        r = ssh_bind_accept(sshbind, session);
        pid_t pid;
        if ((pid = fork()) == 0)
        {
            printf("[INFO] Binded connection to port.\n");
            if (r == SSH_ERROR)
            {
                printf("[ERROR] Connection could not be accepted: %s.\n", ssh_get_error(sshbind));
                return 1;
            }
            if (ssh_handle_key_exchange(session))
            {
                printf("[INFO] Failed to handle key exchange: %s.\n", ssh_get_error(session));
                return 1;
            }

            /* proceed to authentication */
            auth = authenticate(session);
            if (!auth)
            {
                printf("[ERROR] Failed to authenticate: %s.\n", ssh_get_error(session));
                ssh_disconnect(session);
                return 1;
            }

            /* wait for a channel session */
            do
            {
                message = ssh_message_get(session);
                if (message)
                {
                    if (ssh_message_type(message) == SSH_REQUEST_CHANNEL_OPEN &&
                        ssh_message_subtype(message) == SSH_CHANNEL_SESSION)
                    {
                        chan = ssh_message_channel_request_open_reply_accept(message);
                        ssh_message_free(message);
                        break;
                    }
                    else
                    {
                        ssh_message_reply_default(message);
                        ssh_message_free(message);
                    }
                }
                else
                {
                    break;
                }
            } while (!chan);

            if (!chan)
            {
                printf("[ERROR] Client did not request a channel session (%s).\n",
                       ssh_get_error(session));
                ssh_finalize();
                return 1;
            }

            /* wait for a shell */
            do
            {
                message = ssh_message_get(session);
                if (message != NULL)
                {
                    if (ssh_message_type(message) == SSH_REQUEST_CHANNEL)
                    {
                        if (ssh_message_subtype(message) == SSH_CHANNEL_REQUEST_SHELL)
                        {
                            shell = 1;
                            ssh_message_channel_request_reply_success(message);
                            ssh_message_free(message);
                            break;
                        }
                        else if (ssh_message_subtype(message) == SSH_CHANNEL_REQUEST_PTY)
                        {
                            ssh_message_channel_request_reply_success(message);
                            ssh_message_free(message);
                            continue;
                        }
                    }
                    ssh_message_reply_default(message);
                    ssh_message_free(message);
                }
                else
                {
                    break;
                }
            } while (!shell);

            if (!shell)
            {
                printf("[ERROR] Client did not ask for a shell (%s).\n", ssh_get_error(session));
                return 1;
            }

            printf("[INFO] Connected to a client.\n");

            main_loop(chan);

            ssh_disconnect(session);
            ssh_bind_free(sshbind);
            exit(0);
        }
        else if (pid == -1)
        {
            printf("[ERROR] Fork error. Can not run client session in child process.\n");
        }
    }
    ssh_finalize();
    return 0;
}

void sigchld_handler(int sig)
{
    // get information about the child
    int exit_status;
    pid_t pid;

    // keep getting children
    while ((pid = waitpid(-1, &exit_status, WNOHANG | WUNTRACED)) > 0)
    {
        if (WIFSTOPPED(exit_status))
        {
            // CASE: process was sent SIGSTP
            printf("[INFO] Child was stopped.\n");
        }
        if (WIFSIGNALED(exit_status))
        {
            // CASE: process was sent SIGINT
            printf("[INFO] Child was interrupted.\n");
        }
        else if (WIFEXITED(exit_status))
        {
            // CASE: process exited normally
            printf("[INFO] Child exited normally.\n");
        }
    }
}

int main(int argc, char **argv)
{
    // set the handler for exited children
    signal(SIGCHLD, sigchld_handler);
    // run the server daemon
    run_server(argc, argv);
}

